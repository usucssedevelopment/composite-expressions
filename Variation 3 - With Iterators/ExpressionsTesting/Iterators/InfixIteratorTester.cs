﻿using System.Collections.Generic;
using Microsoft.VisualStudio.TestTools.UnitTesting;

using Expressions;
using Expressions.Functions;
using Expressions.Operators;

namespace ExpressionsTesting.Iterators
{
    [TestClass]
    public class InOrderIteratorTester
    {
        [TestMethod]
        public void InfixIterator_TestOnAtomicExpression()
        {
            var exp = new Constant() { Value = 1.2 };

            var i = exp.GetInfixIterator();
            Assert.IsNotNull(i);
            Assert.IsNull(i.Current);
            Assert.IsFalse(i.IsDone);

            // Move next -- should be first and only node
            Assert.IsTrue(i.MoveNext());
            Assert.AreSame(exp, i.Current);
            Assert.IsFalse(i.IsDone);

            // Try to move again
            Assert.IsFalse(i.MoveNext());
            Assert.IsNull(i.Current);
            Assert.IsTrue(i.IsDone);

        }

        [TestMethod]
        public void InfixIterator_TestUnaryExpression()
        {
            var v1 = new Variable() { Label = "A" };
            var exp = new UnaryOperation()
            {
                Operator = new FloorOperator(),
                Operand = v1
            };

            var i = exp.GetInfixIterator();
            Assert.IsNotNull(i);
            Assert.IsFalse(i.IsDone);

            // Move next
            Assert.IsTrue(i.MoveNext());
            Assert.AreSame(exp, i.Current);
            Assert.IsFalse(i.IsDone);

            // Move next
            Assert.IsTrue(i.MoveNext());
            Assert.AreSame(v1, i.Current);
            Assert.IsFalse(i.IsDone);

            // Try to move again
            Assert.IsFalse(i.MoveNext());
            Assert.IsNull(i.Current);
            Assert.IsTrue(i.IsDone);

        }

        [TestMethod]
        public void InfixIterator_TestBinaryExpression()
        {
            var v1 = new Variable() { Label = "A" };
            var v2 = new Variable() { Label = "B" };

            var exp = new BinaryOperation()
            {
                Operator = new AddOperator(),
                Operand1 = v1,
                Operand2 = v2
            };

            var i = exp.GetInfixIterator();
            Assert.IsNotNull(i);
            Assert.IsNull(i.Current);
            Assert.IsFalse(i.IsDone);

            // Move next
            Assert.IsTrue(i.MoveNext());
            Assert.AreSame(v1, i.Current);
            Assert.IsFalse(i.IsDone);

            // Move next
            Assert.IsTrue(i.MoveNext());
            Assert.AreSame(exp, i.Current);
            Assert.IsFalse(i.IsDone);

            // Move next
            Assert.IsTrue(i.MoveNext());
            Assert.AreSame(v2, i.Current);
            Assert.IsFalse(i.IsDone);

            // Try to move again
            Assert.IsFalse(i.MoveNext());
            Assert.IsNull(i.Current);
            Assert.IsTrue(i.IsDone);
        }

        [TestMethod]
        public void InfixIterator_TestFunctionExpression()
        {
            var v1 = new Variable() { Label = "A" };
            var v2 = new Variable() { Label = "B" };
            var v3 = new Variable() { Label = "B" };

            var exp = new FunctionExpression()
            {
                Function = new BigSlowFunction(),
                Parameters = new List<Expression>() { v1, v2, v3 }
            };

            var i = exp.GetInfixIterator();
            Assert.IsNotNull(i);
            Assert.IsNull(i.Current);
            Assert.IsFalse(i.IsDone);

            // Move next
            Assert.IsTrue(i.MoveNext());
            Assert.AreSame(exp, i.Current);
            Assert.IsFalse(i.IsDone);

            // Move next
            Assert.IsTrue(i.MoveNext());
            Assert.AreSame(v1, i.Current);
            Assert.IsFalse(i.IsDone);

            // Move next
            Assert.IsTrue(i.MoveNext());
            Assert.AreSame(v2, i.Current);
            Assert.IsFalse(i.IsDone);

            // Move next
            Assert.IsTrue(i.MoveNext());
            Assert.AreSame(v3, i.Current);
            Assert.IsFalse(i.IsDone);

            // Try to move again
            Assert.IsFalse(i.MoveNext());
            Assert.IsNull(i.Current);
            Assert.IsTrue(i.IsDone);
        }

        [TestMethod]
        public void InfixIterator_TestExpressionTree()
        {
            var v1 = new Variable() { Label = "A" };
            var v2 = new Variable() { Label = "B" };
            var v3 = new Variable() { Label = "C" };

            var v4 = new Variable() { Label = "D" };
            var v5 = new Variable() { Label = "E" };

            var v7 = new Variable() { Label = "H" };
            var v8 = new Variable() { Label = "I" };
            var v9 = new Variable() { Label = "J" };

            var exp1 = new FunctionExpression()
            {
                Function = new BigSlowFunction(),
                Parameters = new List<Expression>() { v1, v2, v3 }
            };

            var exp2 = new BinaryOperation()
            {
                Operator = new AddOperator(),
                Operand1 = v4,
                Operand2 = v5
            };

            var exp3 = new FunctionExpression()
            {
                Function = new BigSlowFunction(),
                Parameters = new List<Expression>() { v7, v8, v9 }
            };

            var exp4 = new FunctionExpression()
            {
                Function = new BigSlowFunction(),
                Parameters = new List<Expression>() { exp1, exp2, exp3 }
            };


            var i = exp4.GetInfixIterator();
            Assert.IsNotNull(i);
            Assert.IsFalse(i.IsDone);

            // Move next
            Assert.IsTrue(i.MoveNext());
            Assert.AreSame(exp4, i.Current);
            Assert.IsFalse(i.IsDone);

            // Move next
            Assert.IsTrue(i.MoveNext());
            Assert.AreSame(exp1, i.Current);
            Assert.IsFalse(i.IsDone);

            // Move next
            Assert.IsTrue(i.MoveNext());
            Assert.AreSame(v1, i.Current);
            Assert.IsFalse(i.IsDone);

            // Move next
            Assert.IsTrue(i.MoveNext());
            Assert.AreSame(v2, i.Current);
            Assert.IsFalse(i.IsDone);

            // Move next
            Assert.IsTrue(i.MoveNext());
            Assert.AreSame(v3, i.Current);
            Assert.IsFalse(i.IsDone);

            // Move next
            Assert.IsTrue(i.MoveNext());
            Assert.AreSame(v4, i.Current);
            Assert.IsFalse(i.IsDone);

            // Move next
            Assert.IsTrue(i.MoveNext());
            Assert.AreSame(exp2, i.Current);
            Assert.IsFalse(i.IsDone);

            // Move next
            Assert.IsTrue(i.MoveNext());
            Assert.AreSame(v5, i.Current);
            Assert.IsFalse(i.IsDone);

            // Move next
            Assert.IsTrue(i.MoveNext());
            Assert.AreSame(exp3, i.Current);
            Assert.IsFalse(i.IsDone);

            // Move next
            Assert.IsTrue(i.MoveNext());
            Assert.AreSame(v7, i.Current);
            Assert.IsFalse(i.IsDone);

            // Move next
            Assert.IsTrue(i.MoveNext());
            Assert.AreSame(v8, i.Current);
            Assert.IsFalse(i.IsDone);

            // Move next
            Assert.IsTrue(i.MoveNext());
            Assert.AreSame(v9, i.Current);
            Assert.IsFalse(i.IsDone);

            // Try to move again
            Assert.IsFalse(i.MoveNext());
            Assert.IsNull(i.Current);
            Assert.IsTrue(i.IsDone);
        }

    }
}
