﻿using System;
using Microsoft.VisualStudio.TestTools.UnitTesting;

using Expressions;

namespace ExpressionsTesting
{
    [TestClass]
    public class InterpretationTester
    {
        [TestMethod]
        public void Interpretation_TestEverything()
        {
            var interpretation = new Interpretation();

            var goodVariables = new[]
            {
                new Variable() { Label = "A" },
                new Variable() { Label = "B" },
                new Variable() { Label = "C" },
                new Variable() { Label = "D" },
                new Variable() { Label = "E" },
                new Variable() { Label = "F" },
                new Variable() { Label = "G" },
                new Variable() { Label = "H" },
                new Variable() { Label = "I" },
                new Variable() { Label = "XYZZY" },
                new Variable() { Label = "ABC1234" }
            };

            // Check init set of each variable -- should be 0
            foreach (var v in goodVariables)
                Assert.AreEqual(0, interpretation[v]);

            // Set a variable and make sure the value is remembered
            interpretation[goodVariables[0]] = 123.45;
            Assert.AreEqual(123.45, interpretation[goodVariables[0]]);

            // Set lots of variables
            var r = new Random();
            foreach (var v in goodVariables)
            {
                var value = r.NextDouble();
                interpretation[v] = value;
                Assert.AreEqual(value, interpretation[v]);
            }

            // Check bad variables
            Assert.AreEqual(0, interpretation[null]);
            interpretation[null] = 0;

            var badVariables = new[]
            {
                new Variable(),
                new Variable() { Label = string.Empty },
                new Variable() { Label = "  " }
            };

            // Check value of bad variable -- always should be 0
            foreach (var v in badVariables)
                Assert.AreEqual(0, interpretation[v]);

            // Try to set bad variables -- values should always remain 0
            r = new Random();
            foreach (var v in badVariables)
            {
                var value = r.NextDouble();
                interpretation[v] = value;
                Assert.AreEqual(0, interpretation[v]);
            }

        }
    }
}
