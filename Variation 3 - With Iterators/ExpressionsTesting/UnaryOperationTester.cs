﻿using Microsoft.VisualStudio.TestTools.UnitTesting;

using Expressions;
using Expressions.Operators;

namespace ExpressionsTesting
{
    [TestClass]
    public class UnaryOperationTester
    {
        [TestMethod]
        public void UnaryOperator_TestWithConstant()
        {
            var exp = new UnaryOperation()
            {
                Operand = new Constant() { Value = 12.34 },
                Operator = new FloorOperator()
            };
            var interpretation = new Interpretation();

            Assert.AreEqual(12, exp.Evaluate(interpretation));
            Assert.AreEqual(1, exp.EstimateTime(interpretation));
            Assert.AreEqual(12, exp.EstimateValue(interpretation));
            Assert.AreEqual("\u230B 12.34", exp.PrefixNotation);
            Assert.AreEqual("(\u230B 12.34)", exp.InfixNotation);
            Assert.AreEqual("12.34 \u230B", exp.PostfixNotation);
            var optimized = exp.OptimizedExpression;
            Assert.IsTrue(optimized is Constant);
            Assert.AreEqual(12, ((Constant)optimized).Value);
        }

        [TestMethod]
        public void UnaryOperator_TestWithVariable()
        {
            var interpretation = new Interpretation();

            var a = new Variable() { Label = "a" };
            var exp = new UnaryOperation()
            {
                Operand = a,
                Operator = new CeilingOperator()
            };
            interpretation[a] = 23.45;

            Assert.AreEqual(24, exp.Evaluate(interpretation));
            Assert.AreEqual(2, exp.EstimateTime(interpretation));
            Assert.AreEqual(24, exp.EstimateValue(interpretation));
            Assert.AreEqual("\u2309 a", exp.PrefixNotation);
            Assert.AreEqual("(\u2309 a)", exp.InfixNotation);
            Assert.AreEqual("a \u2309", exp.PostfixNotation);

            var optimized = exp.OptimizedExpression;
            Assert.IsTrue(optimized is UnaryOperation);
            Assert.IsTrue(((UnaryOperation) optimized).Operator is CeilingOperator);
            Assert.IsTrue(((UnaryOperation) optimized).Operand is Variable);
            var variable = ((UnaryOperation) optimized).Operand as Variable;
            if (variable != null)
                Assert.AreEqual("a", variable.Label);
        }

        [TestMethod]
        public void UnaryOperator_TestComplexExpression()
        {
            var interpretation = TestingUtils.SetupInterpretation();

            var exp = new UnaryOperation()
            {
                Operator = new CeilingOperator(),
                Operand = new BinaryOperation()
                {
                    Operator = new MultipleOperator(),
                    Operand1 = new UnaryOperation()
                    {
                        Operator = new CeilingOperator(),
                        Operand = new Variable() { Label = "A" }
                    },
                    Operand2 = new UnaryOperation()
                    {
                        Operator = new FloorOperator(),
                        Operand = new Variable() { Label = "B" }
                    }                        
                }
            };

            Assert.AreEqual(4, exp.Evaluate(interpretation));
            Assert.AreEqual(6, exp.EstimateTime(interpretation));
            Assert.AreEqual(4, exp.EstimateValue(interpretation));
            Assert.AreEqual("\u2309 * \u2309 A \u230B B", exp.PrefixNotation);
            Assert.AreEqual("(\u2309 ((\u2309 A) * (\u230B B)))", exp.InfixNotation);
            Assert.AreEqual("A \u2309 B \u230B * \u2309", exp.PostfixNotation);

            var optimized = exp.OptimizedExpression;
            Assert.AreEqual("A \u2309 B \u230B * \u2309", optimized.PostfixNotation);
        }

        [TestMethod]
        public void UnaryOperator_TestNoOperand()
        {
            var interpretation = new Interpretation();

            var exp = new UnaryOperation()
            {
                Operator = new CeilingOperator()
            };

            Assert.AreEqual(0, exp.Evaluate(interpretation));
            Assert.AreEqual(0, exp.EstimateTime(interpretation));
            Assert.AreEqual(0, exp.EstimateValue(interpretation));

            try
            {
                var tmp = exp.PrefixNotation;
                Assert.Fail($"Expected exceptation not throw for {tmp}");
            }
            catch
            {
                // ignore
            }

            try
            {
                var tmp = exp.InfixNotation;
                Assert.Fail($"Expected exceptation not throw for {tmp}");
            }
            catch
            {
                // ignore
            }

            try
            {
                var tmp = exp.PostfixNotation;
                Assert.Fail($"Expected exceptation not throw for {tmp}");
            }
            catch
            {
                // ignore
            }

            try
            {
                var tmp = exp.OptimizedExpression;
                Assert.Fail($"Expected exceptation not throw for {tmp}");
            }
            catch
            {
                // ignore
            }

        }

        [TestMethod]
        public void UnaryOperation_TestNoOperator()
        {
            var interpretation = new Interpretation();

            var exp = new UnaryOperation()
            {
                Operand = new Variable() { Label = "a" }
            };

            Assert.AreEqual(0, exp.Evaluate(interpretation));
            Assert.AreEqual(0, exp.EstimateTime(interpretation));
            Assert.AreEqual(0, exp.EstimateValue(interpretation));

            try
            {
                var tmp = exp.PrefixNotation;
                Assert.Fail($"Expected exceptation not throw for {tmp}");
            }
            catch
            {
                // ignore
            }

            try
            {
                var tmp = exp.InfixNotation;
                Assert.Fail($"Expected exceptation not throw for {tmp}");
            }
            catch
            {
                // ignore
            }

            try
            {
                var tmp = exp.PostfixNotation;
                Assert.Fail($"Expected exceptation not throw for {tmp}");
            }
            catch
            {
                // ignore
            }

            try
            {
                var tmp = exp.OptimizedExpression;
                Assert.Fail($"Expected exceptation not throw for {tmp}");
            }
            catch
            {
                // ignore
            }

        }

    }
}
