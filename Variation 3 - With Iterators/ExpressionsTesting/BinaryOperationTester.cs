﻿using Microsoft.VisualStudio.TestTools.UnitTesting;

using Expressions;
using Expressions.Operators;

namespace ExpressionsTesting
{
    [TestClass]
    public class BinaryOperationTester
    {
        [TestMethod]
        public void BinaryOperator_TestEmptyExpression()
        {
            var interpretation = TestingUtils.SetupInterpretation();

            // Check initial state
            var exp1 = new BinaryOperation();
            Assert.IsNull(exp1.Operand1);
            Assert.IsNull(exp1.Operand2);
            Assert.IsNull(exp1.Operator);
            Assert.AreEqual(0, exp1.Evaluate(interpretation));
            Assert.AreEqual(0, exp1.EstimateTime(interpretation));
            Assert.AreEqual(0, exp1.EstimateValue(interpretation));
            try
            {
                var tmp = exp1.PrefixNotation;
                Assert.Fail($"Expected exception not thrown for {tmp}");
            }
            catch
            {
                // ignore
            }

            try
            {
                var tmp = exp1.InfixNotation;
                Assert.Fail($"Expected exception not thrown for {tmp}");
            }
            catch
            {
                // ignore
            }

            try
            {
                var tmp = exp1.PostfixNotation;
                Assert.Fail($"Expected exception not thrown for {tmp}");
            }
            catch
            {
                // ignore
            }

            var exp2 = exp1.OptimizedExpression as BinaryOperation;
            Assert.IsNotNull(exp2);
            Assert.AreNotSame(exp1, exp2);
            Assert.IsNull(exp2.Operand1);
            Assert.IsNull(exp2.Operand2);
            Assert.IsNull(exp2.Operator);

        }

        [TestMethod]
        public void BinaryOperator_TestBasicExpression()
        {
            var interpretation = TestingUtils.SetupInterpretation();

            // A basic expression with variables
            BinaryOperator op = new AddOperator();
            var v1 = new Variable() { Label = "A" };
            var v2 = new Variable() { Label = "B" };
            var exp1 = new BinaryOperation()
            {
                Operator = op,
                Operand1 = v1,
                Operand2 = v2
            };
            Assert.AreSame(v1, exp1.Operand1);
            Assert.AreSame(v2, exp1.Operand2);
            Assert.AreSame(op, exp1.Operator);
            Assert.AreEqual(3.33, exp1.Evaluate(interpretation));
            Assert.AreEqual(3, exp1.EstimateTime(interpretation));
            Assert.AreEqual(3.33, exp1.EstimateValue(interpretation));
            Assert.AreEqual("+ A B", exp1.PrefixNotation);
            Assert.AreEqual("(A + B)", exp1.InfixNotation);
            Assert.AreEqual("A B +", exp1.PostfixNotation);

            var exp2 = exp1.OptimizedExpression as BinaryOperation;
            Assert.AreEqual("+ A B", exp2?.PrefixNotation);
        }

        [TestMethod]
        public void BinaryOperator_TestComplexExpression()
        {
            var interpretation = TestingUtils.SetupInterpretation();

            // A basic expression with variables
            var v1 = new Variable() { Label = "A" };
            var v2 = new Variable() { Label = "B" };
            var exp1 = new BinaryOperation()
            {
                Operator = new AddOperator(),
                Operand1 = v1,
                Operand2 = v2
            };

            var v3 = new Variable() { Label = "C" };
            var v4 = new Variable() { Label = "D" };
            var exp2 = new BinaryOperation()
            {
                Operator = new AddOperator(),
                Operand1 = v3,
                Operand2 = v4
            };

            var exp3 = new BinaryOperation()
            {
                Operator = new MultipleOperator(),
                Operand1 = exp1,
                Operand2 = exp2
            };

            Assert.AreEqual((1.11 + 2.22)*(4.44 + 8.88), exp3.Evaluate(interpretation));
            Assert.AreEqual(7, exp3.EstimateTime(interpretation));
            Assert.AreEqual((1.11 + 2.22) * (4.44 + 8.88), exp3.EstimateValue(interpretation));
            Assert.AreEqual("* + A B + C D", exp3.PrefixNotation);
            Assert.AreEqual("((A + B) * (C + D))", exp3.InfixNotation);
            Assert.AreEqual("A B + C D + *", exp3.PostfixNotation);

            var exp4 = exp1.OptimizedExpression as BinaryOperation;
            Assert.AreEqual("* + A B + C D", exp3?.PrefixNotation);
        }

    }
}
