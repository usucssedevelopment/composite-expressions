﻿using Expressions.Operators;
using Microsoft.VisualStudio.TestTools.UnitTesting;

namespace ExpressionsTesting.Operators
{
    [TestClass]
    public class FloorOperatorTester
    {
        [TestMethod]
        public void FloorOperator_TestExecute()
        {
            var op = new FloorOperator();
            Assert.AreEqual(0, op.Execute(0));
            Assert.AreEqual(1, op.Execute(1.6));
            Assert.AreEqual(-2, op.Execute(-1.6));

        }

        [TestMethod]
        public void FloorOperator_TestToString()
        {
            var op = new FloorOperator();
            Assert.AreEqual("\u230B", op.ToString());
        }
    }
}
