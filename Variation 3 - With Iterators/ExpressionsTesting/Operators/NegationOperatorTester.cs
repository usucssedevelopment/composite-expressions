﻿using Expressions.Operators;
using Microsoft.VisualStudio.TestTools.UnitTesting;

namespace ExpressionsTesting.Operators
{
    [TestClass]
    public class NegationOperatorTester
    {
        [TestMethod]
        public void NegationOperator_TestExecute()
        {
            var op = new NegationOperator();
            Assert.AreEqual(0, op.Execute(0));
            Assert.AreEqual(-1.6, op.Execute(1.6));
            Assert.AreEqual(2.0, op.Execute(-2.0));

        }

        [TestMethod]
        public void NegationOperator_TestToString()
        {
            var op = new NegationOperator();
            Assert.AreEqual("\u00AC", op.ToString());
        }
    }
}
