﻿using System.Globalization;

namespace Expressions
{
    public class Constant : Expression
    {
        public double Value { get; set; }

        public override double Evaluate(Interpretation interpretation)
        {
            return Value;
        }

        public override string ToString()
        {
            return Value.ToString(CultureInfo.InvariantCulture);
        }

        public override Expression OptimizedExpression => MemberwiseClone() as Constant;
    }
}
