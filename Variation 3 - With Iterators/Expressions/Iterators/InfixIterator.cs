﻿using System.Collections.Generic;

namespace Expressions.Iterators
{
    /// <summary>
    /// For expressions tree's, in-order travel with have the following meaning:
    ///     Nodes with 1 child: visit child then this node
    ///     Nodes with 2 children: visit first child, this node, then second child
    ///     Nodes with 3+ children: visit this node, then each child.
    /// </summary>
    public class InfixIterator : Iterator
    {
        public InfixIterator(Expression firstNode) : base(firstNode) { }

        protected override void SetupNodeList()
        {
            CurrentNode = 0;

            Iterator operandIterator;
            List<Expression> operandNodes;
            switch (FirstNode.SubExpressions.Count)
            {
                case 0:
                    Nodes.Add(FirstNode);
                    break;

                case 1:
                    FirstNode.LeftNestedExpressionCount++;
                    Nodes.Add(FirstNode);
                    operandIterator = FirstNode.SubExpressions[0].GetInfixIterator();
                    operandNodes = operandIterator.Nodes;
                    operandNodes[operandNodes.Count - 1].RightNestedExpressionCount++;
                    Nodes.AddRange(operandNodes);
                    break;

                case 2:
                    operandIterator = FirstNode.SubExpressions[0].GetInfixIterator();
                    operandNodes = operandIterator.Nodes;
                    operandNodes[0].LeftNestedExpressionCount++;
                    Nodes.AddRange(operandNodes);

                    Nodes.Add(FirstNode);

                    operandIterator = FirstNode.SubExpressions[1].GetInfixIterator();
                    operandNodes = operandIterator.Nodes;
                    operandNodes[operandNodes.Count-1].RightNestedExpressionCount++;
                    Nodes.AddRange(operandNodes);
                    break;

                default:
                    Nodes.Add(FirstNode);
                    for (var i = 0; i< FirstNode.SubExpressions.Count; i++)
                    {
                        operandIterator = FirstNode.SubExpressions[i].GetInfixIterator();
                        operandNodes = operandIterator.Nodes;
                        if (operandNodes.Count > 0)
                        {
                            if ( i==0 )
                                operandNodes[0].LeftNestedExpressionCount++;

                            if (i == FirstNode.SubExpressions.Count - 1)
                                operandNodes[operandNodes.Count-1].RightNestedExpressionCount++;

                            operandNodes[operandNodes.Count-1].IsParameterToFunction = true;
                        }
                        Nodes.AddRange(operandNodes);

                    }
                    break;
            }
        }
    }
}
