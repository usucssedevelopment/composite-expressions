﻿namespace Expressions.Iterators
{
    public class PostfixIterator : Iterator
    {
        public PostfixIterator(Expression firstNode) : base(firstNode) { }

        protected override void SetupNodeList()
        {
            CurrentNode = 0;

            foreach (var subExpression in FirstNode.SubExpressions)
            {
                var operandIterator = subExpression.GetPostfixIterator();
                var operandNodes = operandIterator.Nodes;
                Nodes.AddRange(operandNodes);
            }

            Nodes.Add(FirstNode);
        }
    }
}
