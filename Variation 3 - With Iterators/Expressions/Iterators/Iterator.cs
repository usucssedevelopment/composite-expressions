﻿using System.Collections.Generic;

namespace Expressions.Iterators
{
    public abstract class Iterator
    {
        protected readonly Expression FirstNode;
        protected int CurrentNode = -1;
        private readonly List<Expression> _nodes = new List<Expression>();

        public List<Expression> Nodes
        {
            get
            {
                if (CurrentNode <0 )
                    SetupNodeList();
                return _nodes;
            }
        }

        protected Iterator(Expression firstNode)
        {
            FirstNode = firstNode;
        }

        public Expression Current
        {
            get
            {
                if (CurrentNode < 0) return null;

                return (CurrentNode < Nodes.Count) ? Nodes[CurrentNode] : null;
            }
        }

        public bool IsDone
        {
            get
            {
                if (CurrentNode < 0) return false;
                return (CurrentNode >= Nodes.Count);
            }
        }

        public bool MoveNext()
        {
            if (CurrentNode < 0)
                SetupNodeList();
            else
                CurrentNode++;
            return !IsDone;
        }

        protected abstract void SetupNodeList();
    }
}
