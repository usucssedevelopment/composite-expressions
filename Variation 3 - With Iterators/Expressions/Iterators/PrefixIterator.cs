﻿namespace Expressions.Iterators
{
    public class PrefixIterator : Iterator
    {
        public PrefixIterator(Expression firstNode) : base(firstNode) { }

        protected override void SetupNodeList()
        {
            CurrentNode = 0;
            Nodes.Add(FirstNode);
            foreach (var subExpression in FirstNode.SubExpressions)
            {
                var operandIterator = subExpression.GetPrefixIterator();
                var operandNodes = operandIterator.Nodes;
                Nodes.AddRange(operandNodes);
            }
        }
    }
}
