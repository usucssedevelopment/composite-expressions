﻿using System.Collections.Generic;
using System.Text;
using Expressions.Iterators;

namespace Expressions
{
    public abstract class Expression
    {
        public abstract double Evaluate(Interpretation interpretation);

        public virtual double EstimateTime(Interpretation interpretation)
        {
            return 0;
        }

        public virtual double EstimateValue(Interpretation interpretation)
        {
            return Evaluate(interpretation);
        }

        public string PrefixNotation => FormatPrefixOrPostfixNotation(GetPrefixIterator());

        public string InfixNotation => FormatInfixNotation(GetInfixIterator());
        public string PostfixNotation => FormatPrefixOrPostfixNotation(GetPostfixIterator());
        public abstract Expression OptimizedExpression { get; }

        public Iterator GetPrefixIterator() { return new PrefixIterator(this); }
        public Iterator GetInfixIterator() { return new InfixIterator(this); }
        public Iterator GetPostfixIterator() { return new PostfixIterator(this); }

        public List<Expression> SubExpressions { get; set; } = new List<Expression>();

        public int LeftNestedExpressionCount { get; set; }
        public int RightNestedExpressionCount { get; set; }
        public bool IsParameterToFunction { get; set; }

        protected void AddOperand(Expression exp, int operandNumber)
        {
            if (exp == null || operandNumber <= 0) return;

            GrowSubExpressions(operandNumber);
            SubExpressions[operandNumber - 1] = exp;
        }

        protected void GrowSubExpressions(int targetSize)
        {
            for (var i = SubExpressions.Count; i < targetSize; i++)
                SubExpressions.Add(null);
        }

        private static string FormatPrefixOrPostfixNotation(Iterator iterator)
        {
            var builder = new StringBuilder();
            while (iterator.MoveNext())
            {
                builder.Append(iterator.Current);
                builder.Append(" ");
            }

            return builder.ToString().Trim();
        }

        private static string FormatInfixNotation(Iterator iterator)
        {
            var builder = new StringBuilder();
            while (iterator.MoveNext())
            {
                for (var i = 0; i < iterator.Current.LeftNestedExpressionCount; i++)
                    builder.Append("(");

                if (iterator.Current is BinaryOperation)
                    builder.Append(" ");

                builder.Append(iterator.Current);

                if (iterator.Current is BinaryOperation || iterator.Current is UnaryOperation)
                    builder.Append(" ");

                for (var i = 0; i < iterator.Current.RightNestedExpressionCount; i++)
                    builder.Append(")");

                if (iterator.Current.RightNestedExpressionCount==0 && iterator.Current.IsParameterToFunction)
                    builder.Append(", ");
            }

            return builder.ToString().Trim();
        }
    }
}
