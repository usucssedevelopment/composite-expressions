﻿using Expressions.Operators;

namespace Expressions
{
    public class UnaryOperation : Expression
    {
        public UnaryOperator Operator { get; set; }
        public Expression Operand
        {
            get { return (SubExpressions.Count > 0) ? SubExpressions[0] : null; }
            set { AddOperand(value, 1); }
        }

        public override double Evaluate(Interpretation interpretation)
        {
            if (Operator == null || Operand == null) return 0;

            return Operator.Execute(Operand.Evaluate(interpretation));
        }

        public override double EstimateTime(Interpretation interpretation)
        {
            if (Operator == null || Operand == null) return 0;

            return 1 + Operand.EstimateTime(interpretation);
        }

        public override double EstimateValue(Interpretation interpretation)
        {
            if (Operator == null || Operand == null) return 0;

            return Operator.Execute(Operand.EstimateValue(interpretation));
        }

        public override string ToString()
        {
            return Operator.Label;
        }

        public override Expression OptimizedExpression
        {
            get
            {
                var newOperand = Operand.OptimizedExpression;

                Expression result;
                if (newOperand is Constant)
                {
                    var i = new Interpretation();
                    result = new Constant() { Value = Operator.Execute(Operand.Evaluate(i)) };
                }
                else
                {
                    result = new UnaryOperation()
                        {
                            Operand = newOperand,
                            Operator = Operator
                        };
                }
                return result;
            }
        }
    }
}
