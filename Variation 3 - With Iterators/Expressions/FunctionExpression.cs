﻿using System.Collections.Generic;

using Expressions.Functions;

namespace Expressions
{
    public class FunctionExpression : Expression
    {
        public Function Function { get; set; }
        public List<Expression> Parameters
        {
            get { return SubExpressions; }
            set
            {
                SubExpressions = value ?? new List<Expression>();
            }
        }

        public int Arity => Parameters.Count;

        public override double Evaluate(Interpretation interpretation)
        {
            double result = 0;
            if (Function == null || Parameters.Count <= 0) return result;
            var parameterValues = new List<double>();
            foreach (var param in Parameters)
                parameterValues.Add(param.EstimateValue(interpretation));
            result = Function.Execute(parameterValues);
            return result;
        }

        public override double EstimateTime(Interpretation interpretation)
        {
            double result = 0;
            if (Function == null || Parameters == null || Parameters.Count <= 0) return result;
            double parameterTimeEstimates = 0;
            var parameterValueEstimates = new List<double>();
            foreach (var param in Parameters)
            {
                parameterTimeEstimates += param.EstimateTime(interpretation);
                parameterValueEstimates.Add(param.EstimateValue(interpretation));
            }

            result = parameterTimeEstimates + Function.GetEstimatedTime(parameterValueEstimates);
            return result;
        }

        public override double EstimateValue(Interpretation interpretation)
        {
            double result = 0;
            if (Function == null || Parameters.Count <= 0) return result;
            var parameterValues = new List<double>();
            foreach (var param in Parameters)
                parameterValues.Add(param.EstimateValue(interpretation));

            result = Function.GetEstimatedValue(parameterValues);

            return result;
        }

        public override string ToString()
        {
            return Function.Label;
        }

        public override Expression OptimizedExpression
        {
            get
            {
                var newParameters = new List<Expression>();

                if (Function != null && Parameters.Count > 0)
                {
                    foreach (var param in Parameters)
                        newParameters.Add(param.OptimizedExpression);
                }

                var newExpression = new FunctionExpression()
                                                    {
                                                        Function = Function,
                                                        Parameters = newParameters
                                                    };
                return newExpression;
            }
        }
    }
}
