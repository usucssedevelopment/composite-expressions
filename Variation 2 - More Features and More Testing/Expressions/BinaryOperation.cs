﻿using Expressions.Operators;

namespace Expressions
{
    public class BinaryOperation : Expression
    {
        public BinaryOperator Operator { get; set; }
        public Expression Operand1
        {
            get { return (SubExpressions.Count > 0) ? SubExpressions[0] : null; }
            set { AddOperand(value, 1); }
        }
        public Expression Operand2
        {
            get { return (SubExpressions.Count > 1) ? SubExpressions[1] : null; }
            set { AddOperand(value, 2); }
        }

        public override double Evaluate(Interpretation interpretation)
        {
            double result = 0;
            if (Operator != null && Operand1 != null && Operand2 != null)
            {
                double op1 = Operand1.Evaluate(interpretation);
                double op2 = Operand2.Evaluate(interpretation);
                result = Operator.Execute(op1, op2);
            }
            return result;
        }

        public override double EstimateTime(Interpretation interpretation)
        {
            return 1;
        }

        public override double EstimateValue(Interpretation interpretation)
        {
            return Evaluate(interpretation);
        }

        public override string PrefixNotation => $"{Operator.Label} {Operand1.PrefixNotation} {Operand2.PrefixNotation}";

        public override string InfixNotation => $"({Operand1.PrefixNotation} {Operator.Label} {Operand2.PrefixNotation})";

        public override string PostfixNotation => $"{Operand1.PrefixNotation} {Operand2.PrefixNotation} {Operator.Label}";

        public override Expression OptimizedExpression
        {
            get
            {
                var newOperand1 = Operand1?.OptimizedExpression;
                var newOperand2 = Operand2?.OptimizedExpression;

                Expression result;
                if (newOperand1 is Constant && newOperand2 is Constant)
                {
                    var i = new Interpretation();
                    result = new Constant()
                    {
                        Value = Operator.Execute(newOperand1.Evaluate(i), newOperand2.Evaluate(i))
                    };
                }
                else
                {
                    result = new BinaryOperation()
                    {
                        Operand1 = newOperand1,
                        Operand2 = newOperand2,
                        Operator = Operator
                    };
                }
                return result;
            }
        }
    }
}
