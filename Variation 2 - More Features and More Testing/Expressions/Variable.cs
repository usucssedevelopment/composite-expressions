﻿using System;
using System.Text.RegularExpressions;

namespace Expressions
{
    public class Variable: Expression
    {
        private static readonly Regex ValidLabel = new Regex("^[a-zA-Z_][a-zA-Z_0-9]*$");
        private string _label = string.Empty;

        public string Label
        {
            get { return _label; }
            set
            {
                if (string.IsNullOrWhiteSpace(value))
                    _label = string.Empty;
                else
                {
                    value = value.Trim();
                    if (!ValidLabel.IsMatch(value))
                        throw new ApplicationException("Invalid variable label");
                    else
                        _label = value;
                }
            }
        }

        public override double Evaluate(Interpretation interpretation)
        {
            return interpretation?[this] ?? 0;
        }

        public override double EstimateTime(Interpretation interpretation)
        {
            return 1;
        }

        public override double EstimateValue(Interpretation interpretation)
        {
            return interpretation?[this] ?? 0;
        }

        public override string PrefixNotation => Label;

        public override string InfixNotation => Label;

        public override string PostfixNotation => Label;

        public override Expression OptimizedExpression => MemberwiseClone() as Variable;
    }
}
