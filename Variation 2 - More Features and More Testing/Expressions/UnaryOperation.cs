﻿using Expressions.Operators;

namespace Expressions
{
    public class UnaryOperation : Expression
    {
        public UnaryOperator Operator { get; set; }
        public Expression Operand
        {
            get { return (SubExpressions.Count > 0) ? SubExpressions[0] : null; }
            set { AddOperand(value, 1); }
        }

        public override double Evaluate(Interpretation interpretation)
        {
            double result = 0;
            if (Operator != null && Operand != null)
                result = Operator.Execute(Operand.Evaluate(interpretation));
            return result;
        }

        public override double EstimateTime(Interpretation interpretation)
        {
            double result = 1;
            if (Operator != null && Operand != null)
                result += Operand.EstimateTime(interpretation);
            return result;
        }

        public override double EstimateValue(Interpretation interpretation)
        {
            double result = 0;
            if (Operator != null && Operand != null)
                result = Operand.EstimateValue(interpretation);
            return result;
        }

        public override string PrefixNotation => $"{Operator.Label} {Operand.PrefixNotation}";

        public override string InfixNotation => $"({Operator.Label} {Operand.PrefixNotation})";

        public override string PostfixNotation => $"{Operand.PrefixNotation} {Operator.Label}";

        public override Expression OptimizedExpression
        {
            get
            {
                var newOperand = Operand.OptimizedExpression;

                Expression result;
                if (newOperand is Constant)
                {
                    var i = new Interpretation();
                    result = new Constant() { Value = Operator.Execute(Operand.Evaluate(i)) };
                }
                else
                {
                    result = new UnaryOperation()
                        {
                            Operand = newOperand,
                            Operator = Operator
                        };
                }
                return result;
            }
        }
    }
}
