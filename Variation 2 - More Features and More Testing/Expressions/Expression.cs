﻿using System.Collections.Generic;

namespace Expressions
{
    public abstract class Expression
    {
        public abstract double Evaluate(Interpretation interpretation);
        public abstract double EstimateTime(Interpretation interpretation);
        public abstract double EstimateValue(Interpretation interpretation);
        public abstract string PrefixNotation { get; }
        public abstract string InfixNotation { get; }
        public abstract string PostfixNotation { get; }
        public abstract Expression OptimizedExpression { get; }

        public List<Expression> SubExpressions { get; set; } = new List<Expression>();

        protected void AddOperand(Expression exp, int operandNumber)
        {
            if (exp == null || operandNumber <= 0) return;

            GrowSubExpressions(operandNumber);
            SubExpressions[operandNumber - 1] = exp;
        }

        private void GrowSubExpressions(int targetSize)
        {
            for (var i = SubExpressions.Count; i < targetSize; i++)
                SubExpressions.Add(null);
        }
    }
}
