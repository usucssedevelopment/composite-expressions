﻿using System;
using System.Collections.Generic;
using System.Threading;

namespace Expressions.Functions
{
    /// <summary>
    /// BigSlowFunction
    /// 
    /// This function computes a big number based on one or more numbers, but does slowly.  The actually
    /// mathmatically function is not meaningful.  This function just illustrate a long-running method
    /// whose estimate value is not the same as the real computed value, and the estimated time can be big.
    /// </summary>
    public class BigSlowFunction : Function
    {
        private readonly Random _randomGenerator = new Random();

        public override double Execute(List<double> parameters)
        {
            return ComputeBigNumberSlowly(parameters);
        }

        public override double GetEstimatedTime(List<double> parameters)
        {
            if (parameters == null || parameters.Count == 0) return 0;

            var estimatedTime = 1000;
            for (var i = 0; i < parameters.Count; i++)
                estimatedTime += 1000;

            return estimatedTime;
        }

        public override double GetEstimatedValue(List<double> parameters)
        {
            if (parameters == null || parameters.Count == 0) return 0;

            var estimatedValue = .5;
            foreach (var t in parameters)
                estimatedValue *= t;

            return estimatedValue;
        }

        private double ComputeBigNumberSlowly(IReadOnlyList<double> parameters)
        {
            if (parameters == null || parameters.Count==0) return 0;

            Thread.Sleep(1000);
            var result = _randomGenerator.NextDouble();
            foreach (var t in parameters)
            {
                Thread.Sleep(1000);
                result *= t;
            }

            return result;
        }

    }
}
