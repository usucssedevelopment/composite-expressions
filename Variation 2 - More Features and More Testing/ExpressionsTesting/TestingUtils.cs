﻿using Expressions;

namespace ExpressionsTesting
{
    public static class TestingUtils
    {
        public static Interpretation SetupInterpretation()
        {
            var interpretation = new Interpretation();
            var goodVariables = new Variable[]
            {
                new Variable() { Label = "A" },
                new Variable() { Label = "B" },
                new Variable() { Label = "C" },
                new Variable() { Label = "D" },
                new Variable() { Label = "E" },
                new Variable() { Label = "F" },
                new Variable() { Label = "G" },
                new Variable() { Label = "H" },
                new Variable() { Label = "I" },
            };

            var nextValue = 1.11;
            foreach (var v in goodVariables)
            {
                interpretation[v] = nextValue;
                nextValue *= 2;
            }

            return interpretation;
        }
    }
}
