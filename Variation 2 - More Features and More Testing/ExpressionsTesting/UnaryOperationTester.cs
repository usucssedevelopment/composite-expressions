﻿using Microsoft.VisualStudio.TestTools.UnitTesting;

using Expressions;
using Expressions.Operators;

namespace ExpressionsTesting
{
    [TestClass]
    public class UnaryOperationTester
    {
        [TestMethod]
        public void UnaryOperator_TestEverything()
        {
            // Case 1 - A basic unary expression of a constant

            var exp = new UnaryOperation()
            {
                Operand = new Constant() { Value = 12.34 },
                Operator = new FloorOperator()
            };
            var i = new Interpretation();

            Assert.AreEqual(12, exp.Evaluate(i));
            Assert.AreEqual(1, exp.EstimateTime(i));
            Assert.AreEqual(12.34, exp.EstimateValue(i));
            Assert.AreEqual("\u230B 12.34", exp.PrefixNotation);
            Assert.AreEqual("(\u230B 12.34)", exp.InfixNotation);
            Assert.AreEqual("12.34 \u230B", exp.PostfixNotation);
            var optimized = exp.OptimizedExpression;
            Assert.IsTrue(optimized is Constant);
            Assert.AreEqual(12, ((Constant) optimized).Value);

            // Case 2 - A basic unary expression of a variable

            var a = new Variable() { Label = "a" };
            exp = new UnaryOperation()
            {
                Operand = a,
                Operator = new CeilingOperator()
            };
            i[a] = 23.45;

            Assert.AreEqual(24, exp.Evaluate(i));
            Assert.AreEqual(2, exp.EstimateTime(i));
            Assert.AreEqual(23.45, exp.EstimateValue(i));
            Assert.AreEqual("\u2309 a", exp.PrefixNotation);
            Assert.AreEqual("(\u2309 a)", exp.InfixNotation);
            Assert.AreEqual("a \u2309", exp.PostfixNotation);
            optimized = exp.OptimizedExpression;
            Assert.IsTrue(optimized is UnaryOperation);
            Assert.IsTrue(((UnaryOperation) optimized).Operator is CeilingOperator);
            Assert.IsTrue(((UnaryOperation) optimized).Operand is Variable);
            var variable = ((UnaryOperation) optimized).Operand as Variable;
            if (variable != null)
                Assert.AreEqual("a", variable.Label);

            // Case 3 - A ill-formed with no Operand
            exp = new UnaryOperation()
            {
                Operator = new CeilingOperator()
            };

            Assert.AreEqual(0, exp.Evaluate(i));
            Assert.AreEqual(1, exp.EstimateTime(i));
            Assert.AreEqual(0, exp.EstimateValue(i));

            try
            {
                var tmp = exp.PrefixNotation;
                Assert.Fail($"Expected exceptation not throw for {tmp}");
            }
            catch
            {
                // ignore
            }

            try
            {
                var tmp = exp.InfixNotation;
                Assert.Fail($"Expected exceptation not throw for {tmp}");
            }
            catch
            {
                // ignore
            }

            try
            {
                var tmp = exp.PostfixNotation;
                Assert.Fail($"Expected exceptation not throw for {tmp}");
            }
            catch
            {
                // ignore
            }

            try
            {
                var tmp = exp.OptimizedExpression;
                Assert.Fail($"Expected exceptation not throw for {tmp}");
            }
            catch
            {
                // ignore
            }

            // Case 4 - A ill-formed with no Operator
            exp = new UnaryOperation()
            {
                Operand = a
            };

            Assert.AreEqual(0, exp.Evaluate(i));
            Assert.AreEqual(1, exp.EstimateTime(i));
            Assert.AreEqual(0, exp.EstimateValue(i));

            try
            {
                var tmp = exp.PrefixNotation;
                Assert.Fail($"Expected exceptation not throw for {tmp}");
            }
            catch
            {
                // ignore
            }

            try
            {
                var tmp = exp.InfixNotation;
                Assert.Fail($"Expected exceptation not throw for {tmp}");
            }
            catch
            {
                // ignore
            }

            try
            {
                var tmp = exp.PostfixNotation;
                Assert.Fail($"Expected exceptation not throw for {tmp}");
            }
            catch
            {
                // ignore
            }

            try
            {
                var tmp = exp.OptimizedExpression;
                Assert.Fail($"Expected exceptation not throw for {tmp}");
            }
            catch
            {
                // ignore
            }

        }

    }
}
