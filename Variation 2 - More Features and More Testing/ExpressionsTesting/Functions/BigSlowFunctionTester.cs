﻿using System.Collections.Generic;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Expressions.Functions;

namespace ExpressionsTesting.Functions
{
    [TestClass]
    public class BigSlowFunctionTester
    {
        [TestMethod]
        public void BigSlowFunction_TestWithNullParameters()
        {
            var f = new BigSlowFunction();

            Assert.AreEqual(0, f.Execute(null));
            Assert.AreEqual(0, f.GetEstimatedTime(null));
            Assert.AreEqual(0, f.GetEstimatedValue(null));
        }

        [TestMethod]
        public void BigSlowFunction_TestWithNoParameters()
        {
            var f = new BigSlowFunction();

            Assert.AreEqual(0, f.Execute(null));
            Assert.AreEqual(0, f.GetEstimatedTime(null));
            Assert.AreEqual(0, f.GetEstimatedValue(null));

            var p = new List<double>();
            var r = f.Execute(p);
            Assert.IsTrue(r >= 0 && r < 1);
            Assert.AreEqual(0, f.GetEstimatedTime(p));
            Assert.AreEqual(0, f.GetEstimatedValue(p));
        }


        [TestMethod]
        public void BigSlowFunction_TestWithOneParameter()
        {
            var f = new BigSlowFunction();

            var p = new List<double>() { 10 };
            var r = f.Execute(p);
            Assert.IsTrue(r >= 0 && r < 10);
            Assert.AreEqual(2000, f.GetEstimatedTime(p));
            Assert.AreEqual(5, f.GetEstimatedValue(p));
        }

        [TestMethod]
        public void BigSlowFunction_TestWithTwoParameters()
        {
            var f = new BigSlowFunction();
            var p = new List<double>() { 10, 20 };
            var r = f.Execute(p);
            Assert.IsTrue(r >= 0 && r < 211);
            Assert.AreEqual(3000, f.GetEstimatedTime(p));
            Assert.AreEqual(100, f.GetEstimatedValue(p));
        }

        [TestMethod]
        public void BigSlowFunction_TestWithThreeParameters()
        {
            var f = new BigSlowFunction();
            var p = new List<double>() { 10, 20, 30 };
            var r = f.Execute(p);
            Assert.IsTrue(r >= 0 && r < 6301);
            Assert.AreEqual(4000, f.GetEstimatedTime(p));
            Assert.AreEqual(3000, f.GetEstimatedValue(p));
        }

    }
}
