﻿using Microsoft.VisualStudio.TestTools.UnitTesting;

using Expressions;
using Expressions.Operators;

namespace ExpressionsTesting
{
    /// <summary>
    /// MockExpression
    /// 
    /// To test IExpression, since it is an abstract class, we create a mock specialization of
    /// IExpression and test using that mock.
    /// </summary>
    public class MockExpression : Expression
    {
        public override double Evaluate(Interpretation interpretation)
        {
            return 0;
        }

        public override double EstimateTime(Interpretation interpretation)
        {
            return 0;
        }

        public override double EstimateValue(Interpretation interpretation)
        {
            return 0;
        }

        public override string PrefixNotation => string.Empty;

        public override string InfixNotation => string.Empty;

        public override string PostfixNotation => string.Empty;

        public override Expression OptimizedExpression => this;

        /// <summary>
        /// This method exposes the AddOperand method from the base class, which is overwise protected
        /// </summary>
        public new void AddOperand(Expression exp, int operandNumber)
        {
            base.AddOperand(exp, operandNumber);

        }
    }

    [TestClass]
    public class ExpressionTester
    {
        [TestMethod]
        public void Expression_TestBaseClassFunctionality()
        {
            var e1 = new MockExpression();

            // Check initial state
            Assert.IsNotNull(e1.SubExpressions);
            Assert.AreEqual(0, e1.SubExpressions.Count);

            // Check GrowSubExpression through AddOperand in MockExpression
            var c1 = new Constant() { Value = 12.34 };
            e1.AddOperand(c1, 3);
            Assert.AreEqual(3, e1.SubExpressions.Count);
            Assert.IsNull(e1.SubExpressions[0]);
            Assert.IsNull(e1.SubExpressions[1]);
            Assert.AreSame(c1, e1.SubExpressions[2]);

            var c2 = new Constant() { Value = 23.4 };
            e1.AddOperand(c2, 1);
            Assert.AreEqual(3, e1.SubExpressions.Count);
            Assert.AreSame(c2, e1.SubExpressions[0]);
            Assert.IsNull(e1.SubExpressions[1]);
            Assert.AreSame(c1, e1.SubExpressions[2]);

            var c3 = new Constant() { Value = 34.56 };
            e1.AddOperand(c3, 4);
            Assert.AreEqual(4, e1.SubExpressions.Count);
            Assert.AreSame(c2, e1.SubExpressions[0]);
            Assert.IsNull(e1.SubExpressions[1]);
            Assert.AreSame(c1, e1.SubExpressions[2]);
            Assert.AreSame(c3, e1.SubExpressions[3]);
        }

        [TestMethod]
        public void Expression_TestOptimize()
        {
            var c1 = new Constant() { Value = 2 };
            var c2 = new Constant() { Value = 5 };

            var ex1 = new BinaryOperation() {Operand1 = c1, Operand2 = c2, Operator = new AddOperator()};

            var ex2 = ex1.OptimizedExpression;
            Assert.IsNotNull(ex2);
            Assert.AreEqual(typeof(Constant), ex2.GetType());
            Assert.AreEqual(7, ((Constant) ex2).Value);

            var ex3 = new BinaryOperation()
            {
                Operand1 = ex1,
                Operand2 = new Constant() {Value = 5},
                Operator = new SubtractOperator()
            };

            var ex4 = ex3.OptimizedExpression;
            Assert.IsNotNull(ex4);
            Assert.AreEqual(typeof(Constant), ex4.GetType());
            Assert.AreEqual(2, ((Constant) ex4).Value);


            var ex5 = new BinaryOperation()
            {
                Operand1 = ex3,
                Operand2 = new Constant() { Value = 15 },
                Operator = new MultipleOperator()
            };

            var ex6 = ex5.OptimizedExpression;
            Assert.IsNotNull(ex6);
            Assert.AreEqual(typeof(Constant), ex6.GetType());
            Assert.AreEqual(30, ((Constant) ex6).Value);

            var ex7 = new BinaryOperation()
            {
                Operand1 = ex5,
                Operand2 = new Constant() { Value = 20 },
                Operator = new DivideOperator()
            };

            var ex8 = ex7.OptimizedExpression;
            Assert.IsNotNull(ex8);
            Assert.AreEqual(typeof(Constant), ex8.GetType());
            Assert.AreEqual(1.5, ((Constant) ex8).Value);

            var ex9 = new UnaryOperation()
            {
                Operand = ex7,
                Operator = new FloorOperator()
            };

            var ex10 = ex9.OptimizedExpression;
            Assert.IsNotNull(ex10);
            Assert.AreEqual(typeof(Constant), ex10.GetType());
            Assert.AreEqual(1, ((Constant) ex10).Value);

        }
    }
}
