﻿using Expressions.Operators;
using Microsoft.VisualStudio.TestTools.UnitTesting;

namespace ExpressionsTesting.Operators
{
    [TestClass]
    public class DivideOperatorTester
    {
        [TestMethod]
        public void DivideOperator_TestExecute()
        {
            DivideOperator op = new DivideOperator();
            Assert.AreEqual("/", op.Label);
            Assert.AreEqual(2, op.Execute(6, 3));
            Assert.AreEqual(0, op.Execute(0, 2));
            Assert.AreEqual(0.5, op.Execute(-2, -4));

            try
            {
                op.Execute(3, 0);
                Assert.Fail("Expected exception not thrown");
            }
            catch { }

        }
    }
}
