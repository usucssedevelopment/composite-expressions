﻿using System;
using System.Text.RegularExpressions;

namespace Expressions
{
    public class Variable: IExpression
    {
        private static readonly Regex ValidLabel = new Regex("^[a-zA-Z_][a-zA-Z_0-9]*$");
        private string _label = string.Empty;

        public string Label
        {
            get { return _label; }
            set
            {
                if (string.IsNullOrWhiteSpace(value))
                    _label = string.Empty;
                else
                {
                    value = value.Trim();
                    if (!ValidLabel.IsMatch(value))
                        throw new ApplicationException("Invalid variable label");
                    else
                        _label = value;
                }
            }
        }

        public double Evaluate(Interpretation interpretation)
        {
            return interpretation?[this] ?? 0;
        }

        public double EstimateTime(Interpretation interpretation)
        {
            return 0;
        }

        public double EstimateValue(Interpretation interpretation)
        {
            return interpretation?[this] ?? 0;
        }

        public string PrefixNotation => Label;

        public string InfixNotation => Label;

        public string PostfixNotation => Label;

        public IExpression OptimizedExpression => MemberwiseClone() as Variable;
    }
}
