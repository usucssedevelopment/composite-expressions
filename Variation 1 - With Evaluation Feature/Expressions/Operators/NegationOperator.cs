﻿namespace Expressions.Operators
{
    public class NegationOperator : UnaryOperator
    {
        public override double Execute(double operand)
        {
            return -operand;
        }

        public override string ToString() { return "\u00AC"; }
    }
}
