﻿using System.Collections.Generic;

namespace Expressions
{
    public class Interpretation
    {
        private readonly Dictionary<string, double> _interpretation = new Dictionary<string, double>();
        private readonly object _myLock = new object();

        public bool Contains(Variable variable)
        {
            bool result;
            lock (_myLock)
            {
                result = _interpretation.ContainsKey(variable.Label);
            }
            return result;
        }

        public double this[Variable variable]
        {
            get
            {
                double result = 0;
                lock (_myLock)
                {
                    if (!string.IsNullOrWhiteSpace(variable?.Label) && _interpretation.ContainsKey(variable.Label))
                        result = _interpretation[variable.Label];
                }
                return result;
            }
            set
            {
                if (!string.IsNullOrWhiteSpace(variable?.Label))
                {
                    lock (_myLock)
                    {

                        if (_interpretation.ContainsKey(variable.Label))
                            _interpretation[variable.Label] = value;
                        else
                            _interpretation.Add(variable.Label, value);
                    }
                }
            }
        }

    }
}
