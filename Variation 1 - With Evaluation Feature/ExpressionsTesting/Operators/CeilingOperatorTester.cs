﻿using Expressions.Operators;
using Microsoft.VisualStudio.TestTools.UnitTesting;

namespace ExpressionsTesting.Operators
{
    [TestClass]
    public class CeilingOperatorTester
    {
        [TestMethod]
        public void CeilingOperator_TestExecution()
        {
            var op = new CeilingOperator();
            Assert.AreEqual(0, op.Execute(0));
            Assert.AreEqual(2, op.Execute(1.6));
            Assert.AreEqual(-2, op.Execute(-2.6));

        }

        [TestMethod]
        public void CeilingOperator_TestToString()
        {
            var op = new CeilingOperator();
            Assert.AreEqual("\u2309", op.ToString());
        }
    }
}
