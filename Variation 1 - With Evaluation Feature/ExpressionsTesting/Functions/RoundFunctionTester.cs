﻿using System.Collections.Generic;
using Expressions.Functions;
using Microsoft.VisualStudio.TestTools.UnitTesting;

namespace ExpressionsTesting.Functions
{
    [TestClass]
    public class RoundFunctionTester
    {
        [TestMethod]
        public void RoundFunction_TestExecute()
        {
            var rf = new RoundFunction();
            Assert.AreEqual(0, rf.Execute(null));
            Assert.AreEqual(0, rf.Execute(new List<double>()));
            Assert.AreEqual(0, rf.Execute(new List<double>() { 1.0, 3.3 }));
            Assert.AreEqual(1, rf.Execute(new List<double>() { 1.2 }));
            Assert.AreEqual(2, rf.Execute(new List<double>() { 1.6 }));

        }
    }
}
